(function(){
	'use strict';
	angular
		.module('app.shakes')
		.controller('ChallengeList', ChallengeList);

		ChallengeList.$inject = ['getShakes', 'config', 'loading', '$localStorage', 'favoriteShakes'];

		function ChallengeList(getShakes, config, loading, $localStorage, favoriteShakes){
			console.log('vm shakes '+$localStorage.shakesInChallenge);
			var vm = this;
			var initialList = config.challengeId;
			vm.shakes = $localStorage.shakesInChallenge;
			vm.shakeChallengeUpdate = shakeChallengeUpdate;
			vm.favoritesUpdate = favoritesUpdate;
			var currentDate = new Date();
			checkIfExpired(currentDate);

			function shakeChallengeUpdate(){
				$localStorage.shakesInChallenge = vm.shakes;
				console.log($localStorage.shakesInChallenge);
				console.log($localStorage.shakesInFavorites);
			}

			function checkIfExpired(currentDate){
				vm.shakes.forEach(function(entry) {
					var expirationDate = Date.parse(entry.expirationDate);
					if(expirationDate < currentDate){
						entry["expired"] = true;
					}
					console.log(entry);		
				});
				$localStorage.shakesInChallenge = vm.shakes;
				console.log($localStorage.shakesInChallenge);
			}

			function favoritesUpdate(shakeId){
				favoriteShakes.toggleShakeFromFavorites(shakeId);
				vm.shakes.forEach(function(entry) {
					var entryId = entry.id;
					if(entryId == shakeId){
						if(entry["inFavorites"]){
							entry["inFavorites"] = false;
						}else{
							entry["inFavorites"] = true;
						}
					}
					console.log(entry);		
				});
				$localStorage.shakesInChallenge = vm.shakes;
				console.log($localStorage.shakesInChallenge);
			}
		}
})();