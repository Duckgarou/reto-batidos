(function(){
	'use strict';
	angular
		.module('app.shakes')
		.controller('ShakeDetails', ShakeDetails);

		ShakeDetails.$inject = ['$stateParams', 'getShakes', 'loading', 'config', '$cordovaSocialSharing'];

		function ShakeDetails($stateParams, getShakes, loading, config, $cordovaSocialSharing){
			var vm = this;
			var shakeId = $stateParams.shakeId;
			vm.iosLink = config.iosLink;
			vm.androidLink = config.androidLink;
			vm.shakeId = shakeId;
			vm.challengeDay = $stateParams.challengeDay;
			vm.shakeDetails = [];
			vm.rateApp = rateApp;
			vm.shareAnywhere = shareAnywhere;
			loading.showLoadingOverlay();
			activate(shakeId);

			function activate(shakeId){
				return getShakes.getShakeDetails(shakeId).then(onShakeDetailsComplete, onError);
			}
			
			function onShakeDetailsComplete(response) {
				vm.shakeDetails = response;
				loading.hideLoadingOverlay();
				console.log('Got shake Details');
				console.log(vm.shakeDetails);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}
	        
	        function rateApp(){
				var popupInfo = {};
				popupInfo.title = "Califica Reto Batidos";
				popupInfo.message = "¿Estás disfrutando el reto? Te agradeceríamos nos ayudaras calificando nuestra app";
				popupInfo.cancelButtonLabel = "No, gracias";
				popupInfo.laterButtonLabel = "Recuérdame más al rato";
				popupInfo.rateButtonLabel = "Calificar ahora";
				AppRate.preferences.customLocale = popupInfo;
				 
				AppRate.preferences.openStoreInApp = true;
				 
				AppRate.preferences.storeAppURL.ios = vm.iosLink;
				AppRate.preferences.storeAppURL.android = vm.androidLink;
				 
				AppRate.promptForRating(true);
	        }

	        function shareAnywhere() {
		        $cordovaSocialSharing.share("No dejes de disfrutar esta entretenida y sana forma de cambiar tu vida fácilmente", "¡Únete al reto batido!", "images/reto-batido-share.jpg", "https://mujerholistica.com/");
		    }

		}
})();