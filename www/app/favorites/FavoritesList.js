(function(){
	'use strict';
	angular
		.module('app.favorites')
		.controller('FavoritesList', FavoritesList);

		FavoritesList.$inject = ['getShakes','loading', '$localStorage'];

		function FavoritesList(getShakes, loading, $localStorage){
			var vm = this;
			vm.shakeDetails = [];
			vm.noFavorites = false;
			loading.showLoadingOverlay();
			var inFavorites = $localStorage.shakesInFavorites;
			if(inFavorites != undefined){
				activate(inFavorites);
			}else{
				vm.noFavorites = true;
				loading.hideLoadingOverlay();
			}
			
			

			function activate(inFavorites){
				inFavorites.forEach(function(entry) {
					getShakes.getShakeDetails(entry).then(onShakeDetailsComplete, onError);
				});
				console.log(inFavorites);
				loading.hideLoadingOverlay();
			}
			
			function onShakeDetailsComplete(response) {
				vm.shakeDetails.push(response);
				console.log('Got shake Details');
				console.log(vm.shakeDetails);
	        };

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}
		}
})();
