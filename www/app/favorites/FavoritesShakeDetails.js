(function(){
	'use strict';
	angular
		.module('app.favorites')
		.controller('FavoritesShakeDetails', FavoritesShakeDetails);

		FavoritesShakeDetails.$inject = ['$stateParams', 'getShakes', 'loading'];

		function FavoritesShakeDetails($stateParams, getShakes, loading){
			var vm = this;
			var shakeId = $stateParams.shakeId;
			vm.shakeId = shakeId;
			vm.challengeDay = $stateParams.challengeDay;
			vm.shakeDetails = [];
			loading.showLoadingOverlay();
			activate(shakeId);

			function activate(shakeId){
				return getShakes.getShakeDetails(shakeId).then(onShakeDetailsComplete, onError);
			}
			
			function onShakeDetailsComplete(response) {
				vm.shakeDetails = response;
				loading.hideLoadingOverlay();
				console.log('Got shake Details');
				console.log(vm.shakeDetails);
	        };

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}
		}

})();