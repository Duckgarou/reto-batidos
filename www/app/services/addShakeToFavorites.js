(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('favoriteShakes', favoriteShakes);

	function favoriteShakes($localStorage){
		if($localStorage.shakesInFavorites == undefined){
			var shakesInFavorites = [];
		}else{
			var shakesInFavorites = $localStorage.shakesInFavorites;
		}

		var toggleShakeFromFavorites = function(shakeId){
			var shakeIndex = shakesInFavorites.indexOf(shakeId);
			if (shakeIndex > -1) {
			    shakesInFavorites.splice(shakeIndex, 1);
			}else{
				shakesInFavorites.push(shakeId);
			}
			$localStorage.shakesInFavorites = shakesInFavorites;
			console.log($localStorage.shakesInFavorites);
		};

		return{
			toggleShakeFromFavorites: toggleShakeFromFavorites
		}
	}
})();