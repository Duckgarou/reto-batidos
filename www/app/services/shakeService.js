(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('getShakes', getShakes);

		function getShakes($http){
			var getShakesFromChallenge = function(shakeChallengeID){
				return $http.get('http://www.retobatidosapp.com/app/reto_batidos/2016/wp/wp-json/wp/v2/posts?filter[cat]='+shakeChallengeID)
							.then(function(response){
								console.log(shakeChallengeID);
								return response.data;
							});
			};
			var getShakeDetails = function(shakeId){
				return $http.get('http://www.retobatidosapp.com/app/reto_batidos/2016/wp/wp-json/wp/v2/posts/'+shakeId)
							.then(function(response){
								return response.data;
							});
			};
			return {
				getShakesFromChallenge: getShakesFromChallenge,
				getShakeDetails: getShakeDetails
			}
		}
})();