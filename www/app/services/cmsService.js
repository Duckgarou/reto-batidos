(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('getContent', getContent);

		function getContent($http){
			var getPageContent = function(pageId){
				return $http.get('http://www.retobatidosapp.com/app/reto_batidos/2016/wp/wp-json/wp/v2/pages/'+pageId)
							.then(function(response){
								return response.data;
							});
			}
			return {
				getPageContent: getPageContent
			}
		}
})();