(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('loading', loading);

	function loading($ionicLoading){
		var showLoadingOverlay = function(loaderText) {
			console.log(loaderText);
			if(!loaderText){
				loaderText = 'Cargando...';
			}
	    	$ionicLoading.show({
	      		template: loaderText
	    	});
  		}
  		var hideLoadingOverlay = function(){
    		$ionicLoading.hide();
  		}
  		return {
  			showLoadingOverlay: showLoadingOverlay,
  			hideLoadingOverlay: hideLoadingOverlay
  		}
	}
})();