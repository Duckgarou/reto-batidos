(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('getIngredients', getIngredients);

		function getIngredients($http, $localStorage){

			if($localStorage.itemPurchased == undefined){
				var itemPurchased = [];
			}else{
				var itemPurchased = $localStorage.itemPurchased;
			}

			var toggleIngredientPurchased = function(itemId){
				var itemIndex = itemPurchased.indexOf(itemId);
				if (itemIndex > -1) {
				    itemPurchased.splice(itemIndex, 1);
				}else{
					itemPurchased.push(itemId);
				}
				$localStorage.itemPurchased = itemPurchased;
				console.log($localStorage.itemPurchased);
			};

			var getWeekIngredients = function(weekId){
				return $http.get('http://www.retobatidosapp.com/app/reto_batidos/2016/wp/wp-json/wp/v2/posts?filter[cat]='+weekId+'&filter[posts_per_page]=-1')
							.then(function(response){
								console.log(weekId);
								var ingredients = response.data;
								ingredients.forEach(function(entry) {
									var entryId = entry.id;
									if($localStorage.itemPurchased != undefined){
										var itemIndex = $localStorage.itemPurchased.indexOf(entryId);
										console.log('');
										if(itemIndex != undefined){
											if (itemIndex > -1) {
											    entry["itemPurchased"] = true;
											}
											else{
												entry["itemPurchased"] = false;
											}
										}
									}else{
										entry["itemPurchased"] = false;
									}
									console.log(entry);		
								});

								return ingredients;
							});
			}
			return {
				toggleIngredientPurchased: toggleIngredientPurchased,
				getWeekIngredients: getWeekIngredients
			}
		}
})();