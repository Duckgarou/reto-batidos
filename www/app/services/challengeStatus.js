(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('challengeStatus', challengeStatus);

		function challengeStatus($localStorage, $ionicPopup, getShakes, config, $state){
			var challengeId = config.challengeId;
			var initChallenge = function(){
				$localStorage.challengeInit = true;

				var challengeStartDate = new Date();
				$localStorage.challengeStartDate = challengeStartDate;

				console.log($localStorage.challengeStartDate);

				var shakesInChallenge = [];
				var error;
				var dayIncrement = 1;

				var createShakesChallengeInfo = function(shakesInChallenge){
					function callback () { console.log('all done'); 
						$localStorage.shakesInChallenge = shakesInChallenge;
						$state.go('app.shake-list');
						console.log($localStorage.shakesInChallenge);
					}

					var itemsProcessed = 0;


					shakesInChallenge.forEach(function(entry) {
						var expirationDate = new Date();
						expirationDate.setDate(challengeStartDate.getDate() + dayIncrement);

						var entryId = entry.id;
						console.log(entryId);

						entry["challengeFinished"] = false;
						entry["expirationDate"] = expirationDate;
						dayIncrement++;
						console.log(expirationDate);
						console.log(entry);
						if($localStorage.shakesInFavorites != undefined){
							var shakeIndex = $localStorage.shakesInFavorites.indexOf(entryId);
							console.log('');
							if(shakeIndex != undefined){
								if (shakeIndex > -1) {
								    entry["inFavorites"] = true;
								}
								else{
									entry["inFavorites"] = false;
								}
							}
						}else{
							entry["inFavorites"] = false;
						}
						itemsProcessed++;
					    if(itemsProcessed === shakesInChallenge.length) {
					    	callback();
					    }
					});
					
				}
				var onShakeListComplete = function(response) {
					shakesInChallenge = response;
					console.log('Got all the shakes at the beginning');
					console.log(shakesInChallenge);
					createShakesChallengeInfo(shakesInChallenge);
		        };

				var onError = function(err){
		            error = 'Sorry Mario, but the princess is in another castle =( ...';
					console.log(err + ' ' + error);

		            $ionicPopup.alert({
		            	title: 'Ha ocurrido un error',
		             	content: 'Recuerda que para iniciar el reto se necesita una conexión a internet, por lo que si tu conexión está fallando, te sugerimos intentar de nuevo más adelante'
		            }).then(function(res) {
		            	console.log('Test Alert Box');
		            });

				};

				var activate = function(challengeId){
					getShakes.getShakesFromChallenge(challengeId).then(onShakeListComplete, onError);
				}

				activate(challengeId);
			};

			var resetChallenge = function(){
				var confirmChallengeReset = $ionicPopup.confirm({
					title: 'Estás a punto de reiniciar el reto',
					template: 'Si reinicias el reto, vas a perder todo tu progreso. ¿Estás seguro de querer reiniciar?'
				});
				confirmChallengeReset.then(function(res) {
					if(res) {
						console.log('It\'s done...');
						initChallenge();
					} 
				});
			};
			return {
				initChallenge: initChallenge,
				resetChallenge: resetChallenge
			}
		}

})();