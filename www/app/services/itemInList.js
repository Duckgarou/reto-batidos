(function(){
	'use strict';
	angular
		.module('app.core')
		.factory('itemInList', itemInList);

	function itemInList($localStorage){
		if($localStorage.itemsInList == undefined){
			var itemsInList = [];
		}else{
			var itemsInList = $localStorage.itemsInList;
		}

		var toggleItemInList = function(itemId){
			var itemIndex = itemsInList.indexOf(itemId);
			if (itemIndex > -1) {
			    itemsInList.splice(itemIndex, 1);
			}else{
				itemsInList.push(itemId);
			}
			$localStorage.itemsInList = itemsInList;
			console.log($localStorage.itemsInList);
		};

		return{
			toggleItemInList: toggleItemInList
		}
	}
})();