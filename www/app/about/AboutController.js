(function(){
	'use strict';
	angular
		.module('app.about')
		.controller('AboutController', AboutController);

		AboutController.$inject = ['$stateParams', 'getContent', 'loading'];

		function AboutController($stateParams, getContent, loading){
			var vm = this;
			var pageId = $stateParams.pageId;
			vm.pageId = pageId;
			vm.pageDetails = [];
			vm.rateApp = rateApp;
			loading.showLoadingOverlay();
			activate(pageId);

			function activate(pageId){
				return getContent.getPageContent(pageId).then(onPageDetailsComplete, onError);
			}
			
			function onPageDetailsComplete(response) {
				vm.pageDetails = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.pageDetails);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}
        
	        function rateApp(){
				var popupInfo = {};
				popupInfo.title = "Califica Reto Batidos";
				popupInfo.message = "¿Estás disfrutando el reto? Te agradeceríamos nos ayudaras calificando nuestra app";
				popupInfo.cancelButtonLabel = "No, gracias";
				popupInfo.laterButtonLabel = "Recuérdame más al rato";
				popupInfo.rateButtonLabel = "Calificar ahora";
				AppRate.preferences.customLocale = popupInfo;
				 
				AppRate.preferences.openStoreInApp = true;
				 
				AppRate.preferences.storeAppURL.ios = vm.iosLink;
				// AppRate.preferences.storeAppURL.android = 'vm.androidLink';
				 
				AppRate.promptForRating(true);
	        }


		}

})();