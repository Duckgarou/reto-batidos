(function() {
    'use strict';
    angular.module('app', [
		/* Everybody has access to these */
        'app.core',

        /* Feature areas */
        'app.menu',
        'app.footer',
        'app.home',
        'app.about',
        'app.shopping',
        'app.faq',
        'app.connect',
        'app.ingredients',
        'app.wyn',
        'app.previous.challenges',
        'app.favorites',
        'app.shakes'
    ]);
})();