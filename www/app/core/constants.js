(function(){
	'use strict';
	angular
		.module('app.core')
		.constant('config',{
			challengeId : 4,
			prevChallengesId : 5,
			ingredientsWeek1: 7,
			ingredientsWeek2: 8,
			iosLink: '1099550582',
			androidLink: 'market://details?id=com.neopixel.retobatido'
		});
})();