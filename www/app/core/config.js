(function(){'use strict';
	angular.module('app.core')
		.run(function($ionicPlatform) {
			$ionicPlatform.ready(function() {
	    	// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
	    	// for form inputs)
	    	if (window.cordova && window.cordova.plugins.Keyboard) {
	      		cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
	      		cordova.plugins.Keyboard.disableScroll(true);

	    	}
	   		if (window.StatusBar) {
	      		// org.apache.cordova.statusbar required
	      		StatusBar.styleDefault();
	    		}
	  		});
		})
		.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
	  		$stateProvider

	    		.state('app', {
	    			url: '/app',
	    			abstract: true,
	    			templateUrl: 'app/layout/menu.html'
	  			})

	  			.state('app.home', {
	    			url: '/home',
	   				views: {
	      				'menuContent': {
	        				templateUrl: 'app/home/home.html'
	      				}
	    			},
	    			cache: false
	  			})
		        .state('app.shake-list', {
		          	url: '/shake-list',
		          	views: {
		            	'menuContent': {
		             		templateUrl: 'app/shakes/challenge-list.html'
		            	}
		          	}
		        })
		        .state('app.shake-details', {
		          	url: '/shake-list/:challengeDay/:shakeId',
		          	views: {
		              	'menuContent': {
		                	templateUrl: 'app/shakes/shake-details.html'
		            	}
		        	}
	        	})
		        .state('app.previous-shake-list', {
		          	url: '/previous-shake-list',
		          	views: {
		            	'menuContent': {
		             		templateUrl: 'app/previous-challenges/challenge-list.html'
		            	}
		          	}
		        })
		        .state('app.previous-shake-details', {
		          	url: '/previous-shake-list/:shakeId',
		          	views: {
		              	'menuContent': {
		                	templateUrl: 'app/previous-challenges/shake-details.html'
		            	}
		        	}
	        	})
		        .state('app.favorites-shake-list', {
		          	url: '/favorites-shake-list',
		          	views: {
		            	'menuContent': {
		             		templateUrl: 'app/favorites/favorites-list.html'
		            	}
		          	}
		        })
		        .state('app.favorites-shake-details', {
		          	url: '/favorites-shake-list/:shakeId',
		          	views: {
		              	'menuContent': {
		                	templateUrl: 'app/favorites/shake-details.html'
		            	}
		        	}
	        	})
		        .state('app.about', {
		          	url: '/about/:pageId',
		          	views: {
		              	'menuContent': {
		                	templateUrl: 'app/about/about.html'
		            	}
		        	}
	        	})
		        .state('app.shopping', {
		          	url: '/shopping-list/:pageId',
		          	views: {
		              	'menuContent': {
		                	templateUrl: 'app/shopping-list/shopping.html'
		            	}
		        	}
	        	})
		        .state('app.connect', {
		          	url: '/connect',
		          	views: {
		              	'menuContent': {
		                	templateUrl: 'app/connect/connect.html'
		            	}
		        	}
	        	})
		        .state('app.faq', {
		          	url: '/faq/:pageId',
		          	views: {
		              	'menuContent': {
		                	templateUrl: 'app/faq/faq.html'
		            	}
		        	}
	        	})
		        .state('app.ingredients', {
		          	url: '/ingredients/:pageId',
		          	views: {
		              	'menuContent': {
		                	templateUrl: 'app/ingredients/ingredients.html'
		            	}
		        	}
	        	})
		        .state('app.wyn', {
		          	url: '/what-you-need/:pageId',
		          	views: {
		              	'menuContent': {
		                	templateUrl: 'app/what-you-need/wyn.html'
		            	}
		        	}
	        	});
	  			// if none of the above states are matched, use this as the fallback
	 	 	$urlRouterProvider.otherwise('/app/home');
	 	 	$ionicConfigProvider.views.maxCache(0);
	 	 	$ionicConfigProvider.backButton.text('Regresar').icon('ion-chevron-left');
	 	 	
		});
})();