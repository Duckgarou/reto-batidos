(function() {
    'use strict';

    angular.module('app.core', [
        /* Angular dependencies */
        /* Third party modules */
        'ngStorage',
        'ngCordova',
        'ngResource',
        'mailchimp',
        'ionic'
    ]);
})();
