(function(){
	'use strict';
	angular
		.module('app.previous.challenges')
		.controller('PreviousChallengeList', PreviousChallengeList);

		PreviousChallengeList.$inject = ['getShakes', 'config', 'loading', '$localStorage', 'favoriteShakes'];

		function PreviousChallengeList(getShakes, config, loading, $localStorage, favoriteShakes){
			var vm = this;
			var initialList = config.prevChallengesId;
			vm.shakesInChallenge = [];
			vm.favoritesUpdate = favoritesUpdate;
			activate(initialList);
			
			function activate(challengeId){
				getShakes.getShakesFromChallenge(challengeId).then(onShakeListComplete, onError);
			}

			function onShakeListComplete(response) {
				vm.shakesInChallenge = response;
				console.log('Got all the shakes at the beginning');
				console.log(vm.shakesInChallenge);
			}

			function onError(err){
		        error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(error);
			}

			function favoritesUpdate(shakeId){
				favoriteShakes.toggleShakeFromFavorites(shakeId);
				vm.shakesInChallenge.forEach(function(entry) {
					var entryId = entry.id;
					if(entryId == shakeId){
						if(entry["inFavorites"]){
							entry["inFavorites"] = false;
						}else{
							entry["inFavorites"] = true;
						}
					}
					console.log(entry);		
				});
				$localStorage.shakesInChallenge = vm.shakes;
				console.log($localStorage.shakesInChallenge);
			}

		}
})();