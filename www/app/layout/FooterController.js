(function(){
	'use strict';
	angular
		.module('app.footer')
		.controller('FooterController', FooterController);

		FooterController.$inject = ['$ionicPlatform', '$cordovaSocialSharing', 'config'];

		function FooterController($ionicPlatform, $cordovaSocialSharing, config){
			var vm = this;
			vm.rateApp = rateApp;
			vm.shareAnywhere = shareAnywhere;
			vm.iosLink = config.iosLink;
			vm.androidLink = config.androidLink;
			
	        function rateApp(){
				var popupInfo = {};
				popupInfo.title = "Califica Reto Batidos";
				popupInfo.message = "¿Estás disfrutando el reto? Te agradeceríamos nos ayudaras calificando nuestra app";
				popupInfo.cancelButtonLabel = "No, gracias";
				popupInfo.laterButtonLabel = "Recuérdame más al rato";
				popupInfo.rateButtonLabel = "Calificar ahora";
				AppRate.preferences.customLocale = popupInfo;
				 
				AppRate.preferences.openStoreInApp = true;
				 
				AppRate.preferences.storeAppURL.ios = vm.iosLink;
				AppRate.preferences.storeAppURL.android = vm.androidLink;
				 
				AppRate.promptForRating(true);
	        }

	        function shareAnywhere() {
		        $cordovaSocialSharing.share("No dejes de disfrutar esta entretenida y sana forma de cambiar tu vida fácilmente", "¡Únete al reto batido!", "images/reto-batido-share.jpg", "https://mujerholistica.com/");
		    }
		}

})();