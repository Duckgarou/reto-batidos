(function(){
	'use strict';
	angular
		.module('app.menu')
		.controller('MenuController', MenuController);

		MenuController.$inject = ['challengeStatus', '$localStorage', '$ionicPopup'];

		function MenuController(challengeStatus, $localStorage, $ionicPopup){
			var vm = this;

			vm.challengeInit = $localStorage.challengeInit;
			vm.menuButtonBehavior = menuButtonBehavior;

			function menuButtonBehavior(){
				console.log('Stuff');
				if(!vm.challengeInit){
					var alertPopup = $ionicPopup.alert({
	     				title: 'No has iniciado un reto',
	     				template: 'Recuerda que para ver los batidos, tienes qué iniciar el reto'
	   				})
	   				.then(function(res) {
		   				console.log('falso '+vm.challengeInit);
		   				return false;
					});
				}else{
					console.log(vm.challengeInit);
				}
			}
		}

})();