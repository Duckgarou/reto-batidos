(function(){
	'use strict';
	angular
		.module('app.shopping')
		.controller('ShoppingController', ShoppingController);

		ShoppingController.$inject = ['$stateParams', '$localStorage', 'getContent', 'loading', 'config', 'getIngredients'];

		function ShoppingController($stateParams, $localStorage, getContent, loading, config, getIngredients){
			var vm = this;
			var week1 = config.ingredientsWeek1;
			var week2 = config.ingredientsWeek2;
			vm.ingredientsWeek1 = [];
			vm.ingredientsWeek2 = [];
			vm.toggleIngredientPurchased = getIngredients.toggleIngredientPurchased;
			loading.showLoadingOverlay();
			activateWeek1(week1);
			activateWeek2(week2);

			function activateWeek1(weekId){
				return getIngredients.getWeekIngredients(weekId).then(onWeek1IngredientsComplete, onError);
			}
			function activateWeek2(weekId){
				return getIngredients.getWeekIngredients(weekId).then(onWeek2IngredientsComplete, onError);
			}
			
			function onWeek1IngredientsComplete(response) {
				vm.ingredientsWeek1 = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.ingredientsWeek1);
	        }

			function onWeek2IngredientsComplete(response) {
				vm.ingredientsWeek2 = response;
				loading.hideLoadingOverlay();
				console.log('Got Page Details');
				console.log(vm.ingredientsWeek2);
	        }

			function onError(err){
	            vm.error = 'Sorry Mario, but the princess is in another castle =( ...';
				console.log(vm.error);
			}

		}

})();