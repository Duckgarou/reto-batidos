(function(){
	'use strict';
	angular
		.module('app.home')
		.controller('HomeController', HomeController);

		HomeController = ['challengeStatus', '$localStorage'];

		function HomeController(challengeStatus, $localStorage){
			var vm = this;
			vm.initChallenge = challengeStatus.initChallenge;
			vm.resetChallenge = challengeStatus.resetChallenge;
			vm.challengeInit = $localStorage.challengeInit;
		}

})();