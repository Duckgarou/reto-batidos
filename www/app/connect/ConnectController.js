(function(){
	'use strict';
	angular
		.module('app.connect')
		.controller('ConnectController', ConnectController);

		ConnectController.$inject = ['$http'];

		function ConnectController($http){
			var vm = this;
			vm.sendEmail = sendEmail;
			vm.mailStatus = false;
			
			function sendEmail(mail){
				console.log(mail);
				var config = {
				    method:'POST',
				    url:'http://neopixel.com.mx/mailchimp/mail.php',
				    data:'userEmail='+mail,
				    headers:{'Content-Type':'application/x-www-form-urlencoded'}
				}
			    var response = $http(config);

			    response.success(function(data,status){
			        console.log('Done');
					vm.mailStatus = true;
					vm.mailMessage = data;
					console.log(vm.mailMessage);
			    });
			    response.error(function(data,status){
			        console.log('Error');
					vm.mailStatus = true;
					vm.mailMessage = data;
					console.log(vm.mailMessage);
			    });
			}
		}
})();