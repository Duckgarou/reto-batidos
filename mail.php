<?php
	// Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }
    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }
	include('mailchimp-api/MailChimp.php'); 
	use \DrewM\MailChimp\MailChimp;

	$userMail = $_POST["userEmail"];
	$MailChimp = new MailChimp('fee69abbfb4e88f7a684700c80f3fda3-us13');
	$list_id = '5cdbf8d73f';

	$result = $MailChimp->post("lists/$list_id/members", [
	                'email_address' => $userMail,
	                'status'        => 'subscribed',
	            ]);

	echo 'Tus datos nos han llegado, ¡bienvenido al newsletter del Reto Batido!';
?>