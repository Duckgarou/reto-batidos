# Reto Batidos App #

App made for the Healthy Shake Challenge

### What is this repository for? ###
* This is an app for people that have an interest in keeping themselves healthy and are fans of the shakes.
* With this app, the user can start a challenge that lasts for 30 days, record his progress and it gives him the chance to save his favorite shakes so he can prepare them anytime he wants!
* Also, a list of ingredients and acceptable substitutes for them are available, giving the chance to create a buying list with all the ingredients necessary for their shakes
* This app was made using ionic and angular as the base, and it's available for IOS and Android
* Version 1.0